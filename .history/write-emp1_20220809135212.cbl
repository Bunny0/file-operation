       IDENTIFICATION DIVISION.
       PROGRAM-ID. WRITE-EMP1.
       AUTHOR. SITTHICHAI.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT EMP-FILE ASSIGN TO "emp1.dat"
           ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION.
       FILE SECTION.
       FD EMP-FILE.
       01 EMP-DETAILS.
           88 END-OF-EMP-FILE   VALUE HIGH-VALUE.
           05 EMP-SSH           PIC   9(9).
           05 EMP-NAME.          
              10 EMP-SURNAME PIC   X(15).
              10 EMP-FORNAME PIC   X(10).
           05 EMP-DATE-OF-BIRTH.
              10 EMP-YOB
              10 EMP-
           05 EMP-GENDER        PIC   X.

       PROCEDURE DIVISION.
       BEGIN.
           OPEN OUTPUT EMP-FILE
           MOVE  "123456789" TO EMP-SSH
           MOVE  "SITTHICHAI" TO EMP-NAME
           MOVE  "20010324" TO EMP-DATE-OF-BIRTH
           MOVE  "M" TO EMP-GENDER
           WRITE EMP-DETAILS

           MOVE  "987654321" TO EMP-SSH
           MOVE  "TONGPRATEANG" TO EMP-NAME
           MOVE  "24032001" TO EMP-DATE-OF-BIRTH
           MOVE  "F" TO EMP-GENDER
           WRITE EMP-DETAILS
           CLOSE EMP-FILE
           GOBACK
           .
