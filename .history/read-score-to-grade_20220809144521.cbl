       IDENTIFICATION DIVISION.
       PROGRAM-ID. READ-SCORE-TO-GREADE.
       AUTHOR. SITTHICHAI.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT SCORE-FILE ASSIGN TO "score.dat"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD  SCORE-FILE.
       01  SCORE-DETAIL.
           88 END-OF-SCORE-FILE VALUE HIGH-VALUE.
           05 STU-ID         PIC   X(8).
           05 MIDTERM-SCORE  PIC   9(2)V9(2).
           05 FINAL-SCORE    PIC   9(2)V9(2).
           05 PROJECT-SCORE  PIC   9(2)V9(2).
       WORKING-STORAGE SECTION.
       01  SUM-SCORE   PIC  9(3)V9(2).
       01  GRADE       PIC   X(2).       
       PROCEDURE DIVISION.
       000-BEGIN.
           OPEN INPUT SCORE-FILE

           PERFORM UNTIL END-OF-SCORE-FILE
              READ SCORE-FILE
                 AT END SET END-OF-SCORE-FILE TO TRUE                
              END-READ
              IF NOT END-OF-SCORE-FILE THEN
                 PERFORM 001-PROCESS THRU 001-EXIT
              END-IF
           END-PERFORM  

           CLOSE SCORE-FILE
           GOBACK 
           .
       001-PROCESS.
           COMPUTE SUM-SCORE = MIDTERM-SCORE + FINAL-SCORE
                              + PROJECT-SCORE
           DISPLAY STU-ID " " SUM-SCORE 
           EVALUATE TRUE
              WHEN  SUM-SCORE>=
           END-EVALUATE
           .
       001-EXIT.
           EXIT.