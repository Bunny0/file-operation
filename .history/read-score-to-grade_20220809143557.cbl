       IDENTIFICATION DIVISION.
       PROGRAM-ID. READ-SCORE-TO-GREADE.
       AUTHOR. SITTHICHAI.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT SCORE-FILE ASSIGN TO "score.dat"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD  SCORE-FILE.
       01  SCORE-DETAIL.
           88 END-OF-SCORE-FILE VALUE HIGH-VALUE.
           05 STU-ID         PIC   X(8).
           05 MIDTERM-SCORE  PIC   9(2)V9(2).
           05 FINAL-SCORE    PIC   9(2)V9(2).
           05 PROJECT-SCORE  PIC   9(2)V9(2).

       PROCEDURE DIVISION.
       BEGIN.
           OPEN INPUT SCORE-FILE

           PERFORM UNTIL END-OF-SCORE-FILE
              RED SCORE-FILE
                 AT END 
              END-READ
           END-PERFORM   
           CLOSE SCORE-FILE
           GOBACK 
       .
