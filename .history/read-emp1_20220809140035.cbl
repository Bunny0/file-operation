       IDENTIFICATION DIVISION.
       PROGRAM-ID. WRITE-EMP1.
       AUTHOR. SITTHICHAI.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT.
       FILE-CONTROL.
           SELECT EMP-FILE ASSIGN TO "emp1.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION.
       FILE SECTION.
       FD  EMP-FILE.
       01 EMP-DETAILS.
           88 END-OF-EMP-FILE   VALUE HIGH-VALUE.
           05 EMP-SSH           PIC   9(9).
           05 EMP-NAME.          
              10 EMP-SURNAME PIC   X(15).
              10 EMP-FORNAME PIC   X(15).
           05 EMP-DATE-OF-BIRTH.
              10 EMP-YOB  PIC   9(4).
              10 EMP-MOB  PIC   9(2).
              10 EMP-DOB  PIC   9(2).
           05 EMP-GENDER        PIC   X.
       PROCEDURE DIVISION.
       BEGIN.
           OPEN INPUT EMP-FILE

           CLOSE EMP-FILE
           GOBACK
           .
           