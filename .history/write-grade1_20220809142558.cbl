       IDENTIFICATION DIVISION.
       PROGRAM-ID. WRITE-EMP1.
       AUTHOR. SITTHICHAI.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION
       FILE-CONTROL.
           SECTION SCORE-FILE ASSIGN TO "grade.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
       
       DATA DIVISION.
       FILE SECTION.
       FD  SCORE-FILE.
       01  GRADE-DETAIL.
           05 STU-ID         PIC   X(8).
           05 MIDTERM-SCORE  PIC   9(2)V9(2).
           05 FINAL-SCORE    PIC   9(2)V9(2).
           05 PROJECT-SCORE  PIC   9(2)V9(2).
       PROCEDURE DIVISION.
       BEGIN.
           OPEN OUTPUT SCORE-FILE
           MOVE "96830193"   TO STU-ID
           MOVE "34.05"   TO MIDTERM-SCORE
           MOVE "25.25"   TO FINAL-SCORE
           MOVE "10.8"    TO PROJECT-SCORE
           CLOSE SCORE-FILE
           GOBACK
           .